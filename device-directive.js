'use strict';

/**
 * @ngdoc directive
 * @name survey.Directives.deviceDirective
 * @description deviceDirective directive
 */
angular
    .module('lsnNgModule')
    .directive('deviceDirective', [

        function () {
            var ua = navigator.userAgent;
            return {
                // name: '',
                // priority: 1,
                // terminal: true,
                // scope: {}, // {} = isolate, true = child, false/undefined = no change
                // controller: function($scope, $element, $attrs, $transclude) {},
                // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
                // restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
                // template: '',
                // templateUrl: '',
                // replace: true,
                // transclude: true,
                // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),


                restrict: 'A',
                scope: {
                    type: '&'
                },

                link: function ($scope, iElm, iAttrs, controller) {
                    /*  if ((ua.match(/iPhone/i)) || (ua.match(/iPod/i)) || (ua.match(/ipad/i)) || (ua.match(/Android/i)) || (ua.match(/blackberry/i))) {
                        $scope.isDevice = true;
                        $scope.type({
                            data: angular.copy($scope.isDevice)
                        });

                    } else {
                        $scope.isDevice = false;
                        $scope.type({
                            data: angular.copy($scope.isDevice)
                        });

                    }*/
                    //var userAgent = 'desktop';
                    var _isMobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
                    if (_isMobile) {
                        var tempUserAgent = navigator.userAgent.toLowerCase();
                        if (((tempUserAgent.search('android') > -1) && (tempUserAgent.search('mobile') > -1)) || (navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {

                            $scope.userAgent = 'mobile';
                            $scope.type({
                                data: angular.copy($scope.userAgent)
                            });

                        } else if ((tempUserAgent.search('android') > -1) && !(tempUserAgent.search('mobile') > -1) || (navigator.userAgent.match(/iPad/i))) {
                            $scope.userAgent = 'tablet';
                            $scope.type({
                                data: angular.copy($scope.userAgent)
                            });
                        }

                    } else {
                        $scope.userAgent = 'desktop';
                        $scope.type({
                            data: angular.copy($scope.userAgent)
                        });
                    }

                }

            }
            }

    ]);